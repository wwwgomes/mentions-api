# mentions-api

Mention API (Tutorial)

Configure .env with DATABASE_CONNECTION_STRING=your_database_connection

## Google Cloud Platform commands (SDK)

| COMMANDS | DESCRIPTION |
| gcloud auth login | Login |
| gcloud config set project PROJECT_ID | Search project by id |
