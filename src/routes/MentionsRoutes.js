const express = require('express');
const { check } = require('express-validator');
const router = express.Router();
const MentionsController = require('../controllers/MentionsController');

router.get('/', MentionsController.list);
router.post(
  '/',
  [
    check('friend')
      .isLength({
        min: 7,
      })
      .withMessage('O nome precisa ter no mínimo 7 caracteres!'),
    check('mention')
      .isLength({
        min: 20,
        max: 280,
      })
      .withMessage(
        'A mensão precisa ter no mínimo 20 e no máximo 280 caracteres!'
      ),
  ],
  MentionsController.create
);

router.put(
  '/:id',
  [
    check('friend')
      .optional()
      .isLength({
        min: 7,
      })
      .withMessage('O nome precisa ter no mínimo 7 caracteres'),
    check('mention')
      .optional()
      .isLength({
        min: 20,
        max: 280,
      })
      .withMessage(
        'A mensão precisa ter no mínimo 20 e no máximo 280 caracteres!'
      ),
  ],
  MentionsController.update
);

router.delete('/:id', MentionsController.delete);

module.exports = router;
