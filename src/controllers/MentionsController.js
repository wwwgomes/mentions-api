const mentionsRepository = require('../repositories/MentionsRepository');
const { validationResult } = require('express-validator');

exports.list = async (req, res) => {
  try {
    const data = await mentionsRepository.list();
    console.log(data);
    res.status(200).send(data);
  } catch (e) {
    res.status(500).send('Falha ao carregar as mensões');
  }
};

exports.create = async (req, res) => {
  const { errors } = validationResult(req);

  if (errors.length > 0) {
    return res.status(400).send({
      menssage: errors,
    });
  }

  try {
    const mention = await mentionsRepository.create({
      friend: req.body.friend,
      mention: req.body.mention,
    });
    console.log(mention);

    res.status(201).send({
      message: `Mensão cadastrada com sucesso ${mention}`,
    });
  } catch (e) {
    res.status(500).send({
      message: 'Falha ao cadastrar a mensão',
    });
  }
};

exports.update = async (req, res) => {
  const { errors } = validationResult(req);

  if (errors.length > 0) {
    return res.status(400).send({
      message: errors,
    });
  }

  try {
    await mentionsRepository.update(req.params.id, req.body);
    return res.status(200).send({
      message: 'Mensão atualizada com sucesso!',
    });
  } catch (e) {
    return res.status(500).send({
      message: 'Falha ao atualizar mensão',
    });
  }
};

exports.delete = async (req, res) => {
  try {
    await mentionsRepository.delete(req.params.id);
    res.status(204).send({
      message: 'Menção removida com sucesso!',
    });
  } catch (e) {
    res.status(500).send({
      message: 'Falha ao remover mensão!',
    });
  }
};
