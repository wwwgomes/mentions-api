const mongoose = require('mongoose');
const Mentions = mongoose.model('Mentions');

exports.list = async () => {
  const res = await Mentions.find({}, 'friend mention -_id');
  return res;
};

exports.create = async data => {
  const mention = new Mentions(data);
  return mention.save();
};

exports.update = async (id, data) => {
  await Mentions.findByIdAndUpdate(id, {
    $set: data,
  });
};

exports.delete = async id => {
  await Mentions.findByIdAndDelete(id);
};
